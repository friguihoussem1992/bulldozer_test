<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Link;

class HistoriqueTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_redirection_historique()
    {
        $user = User::find(1); // find specific user

        $response = $this->actingAs($user)
        ->get('/historique');
        /** Then he should see the data */
        $response
            ->assertStatus(200);
    }



    public function test_creation_short_url()
    {
        $user = User::find(1); // find specific user

        $response = $this->actingAs($user)
        ->post('/create-link',[
            'url'=>'http://google.com'
        ]);
        /** Then he should return home page */
        $response->assertRedirect('/home');
    }


        
    public function test_delete_short_url()
    {
        $user = User::find(1); // find specific user

        $link = Link::orderBy('id')->first(); // find specific user

        $response = $this->actingAs($user)
        ->get("/delete-link/".$link->id);
        /** Then he should return home page */
        $response->assertRedirect('/home');
    }

}
