<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();


 


 
Route::middleware(['auth'])->group(function () {
 
    Route::get('delete-link/{id}',[App\Http\Controllers\LinkController::class, 'destroy']  )->name('destroy_link');
    Route::get('/home', [App\Http\Controllers\LinkController::class, 'index'])->name('home');
    Route::post('create-link',[App\Http\Controllers\LinkController::class, 'store']  )->name('create_link');

    Route::get('/historique', [App\Http\Controllers\HistoriqueController::class, 'index'])->name('home');


});

Route::get('/{code}', [App\Http\Controllers\LinkController::class, 'goToUrl'])->name('redirection');
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);
