<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Link;
use Exception;

class DailyDestroy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'link:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily destroy link after 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{

            $ldate = date('Y-m-d');
            Link::where('created_at','<',$ldate)->delete();
        }  catch (Exception $e) {
          
        }

    }
}
