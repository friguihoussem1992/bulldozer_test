<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historique extends Model
{
    use HasFactory;

    protected $table='historique';
    protected $fillable = [
        'url',
        'adresse_ip',
        'pays',
        'user_agent',
        'identifiant'
    ];
    
    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
