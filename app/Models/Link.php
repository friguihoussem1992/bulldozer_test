<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use HasFactory;

    protected $table='link';
    protected $fillable = [
        'url',
        'url_origin',
        'code'
    ];

    const MAX_ATTEINT=20;
    const MAX_ATTEINT_BY_USER=5;
    
    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    function unique_code($limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }
 

}
