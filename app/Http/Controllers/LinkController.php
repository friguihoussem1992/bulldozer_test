<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Link;
use Exception;

class LinkController extends Controller
{
    //

    public function index(Request $request)
    {

        return view('home')->with('links', self::ListeLinkByUser()); //get list lien
    }
    public function store(Request $request)
    {

        try {
            // validator  

            $validatedData = $request->validate([
                'url' => 'required|max:255',
            ]);
            $user = auth()->user();
            self::checkMaxLinkByUser();
            $Link = new Link();
            $unique_identiant = $Link->unique_code(9); //get unique code pour chaque url 
            $Link->url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $unique_identiant; // create url avec ladresse du serveur 
            $Link->code = $unique_identiant;
            $Link->url_origin = $request->get('url');
            $Link->users()->associate($user);
            $Link->save();

            return redirect()->back()->with('successMsg', __('message.msg_created'));
        } catch (Exception $e) {
            return redirect('home')->with('links', self::ListeLinkByUser())->withErrors($e->getMessage());  //return erreur en cas d'exception
        }
    }

    public function destroy($id)
    {

        $link = Link::findOrFail($id); // if exist else throw exception  
        $link->delete();

        return redirect('home')->with('links', self::ListeLinkByUser())->with('successMsg', __('message.msg_delete'));
    }

    //get la liste des links creer par utilisateur connecté
    public function ListeLinkByUser()
    {
        $user = auth()->user();
        $ListeLink = $user->link;
        return $ListeLink;
    }

    //redirection vers le lien raccourir 
    public function goToUrl(Request $request, $code)
    {

        $link = Link::where('code', $code)->first();
        if ($link) {
            $HistoriqueController = new HistoriqueController();
            $HistoriqueController->CreateHistorique($request);
            return redirect()->away($link->url_origin);
        } else {
            return redirect('/login');
        }
    }

     //methode qui permet de verifier la condition du max atteint par utilisateur connecté 
    public function checkMaxLinkByUser()
    {
        $nbr = count(self::ListeLinkByUser());
        if ($nbr >= Link::MAX_ATTEINT_BY_USER) {
            throw new Exception(__('message.msg_atteint'));
        } else {
            self::checkMaxLinkAll();
        }
    }

    //methode qui permet de verifier la condition du max atteint par tous les utilisateur and delete old query if exist
    public function checkMaxLinkAll()
    {
        $nbr = Link::all()->count();
        if ($nbr >= Link::MAX_ATTEINT) {
            $last_link = self::getOldQuery();
            $last_link->delete();
        }
    }

    //get Old link 
    public function getOldQuery()
    {
        return Link::orderBy('id', 'asc')->first();
    }
}
