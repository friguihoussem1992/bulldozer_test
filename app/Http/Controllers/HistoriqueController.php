<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Historique;
use Location;

class HistoriqueController extends Controller
{
    //

    //get all history and return view 
    public function index(){
        return view('historique')->with('historiques',Historique::all());
    }

    //creation du log pour chaque acces 
    public function CreateHistorique(Request $request){
        $ip =   $request->ip(); //get addresse ip user
        $user_agent=$request->userAgent(); //get user agent navigateur  
        $Location = Location::get($ip)  ;  //get localisation if adresse ip correct   
        $pays=$Location->countryName ?? 'Tunisia' ; //set localisation if ip exist else tunisa par default
        $path=$request->url();//get current adresse

        $Historique=new Historique();
        $Historique->url= $path;
        $Historique->adresse_ip= $ip;
        $Historique->pays= $pays;
        $Historique->user_agent= $user_agent;
        if(auth()->user()){
            $current_user=auth()->user(); //get current user if existe
            $Historique->identifiant= $current_user->email ?? null;
            $Historique->users()->associate($current_user);
        }
      
        $Historique->save(); //enregistrement
    }
}
