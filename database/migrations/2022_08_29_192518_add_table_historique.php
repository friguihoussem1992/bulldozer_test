<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableHistorique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('historique', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string('adresse_ip');
            $table->string('pays');
            $table->string('user_agent');
            $table->string('identifiant')->nullable();

            $table->foreignId('user_id')
            ->nullable()
            ->constrained('users');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
