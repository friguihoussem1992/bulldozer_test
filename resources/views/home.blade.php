@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- <div class="card">
                <div class="card-header">{{ __('message.welcome') }}</div>

              
            </div> -->
            <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif


                </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if(session()->has('successMsg'))
                <div class="alert alert-success">
                    {{ session()->get('successMsg') }}
                </div>
            @endif

            <button class="btn btn-success btn-xs rigth" data-toggle="modal" data-target="#formulaire"> {{ __('message.btn_create_raccourcir') }} </button>
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Url</th>
                        <th>Url origin</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($links as $link)
                    <tr>
                        <td> {{$link->id}}</td>
                        <td> {{$link->url}}</td>
                        <td> {{$link->url_origin}}</td>
                        <td>
                            <a href="{{ url('/delete-link', ['id' => $link->id]) }}" class="btn btn-danger btn-xs">
                                {{__('message.bnt_supprime')}}
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @if(count($links)==0)
                       <tr > <td colspan="4"> Aucune donnée disponible </td></tr>
                    @endif
                </tbody>
            </table>
        </div>


        <div class="modal fade" id="formulaire">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"> {{__('message.title_modal_link')}}</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body row">
                        <form class="col" action="{{ route('create_link') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="nom" class="form-control-label">{{__('message.title_link_label')}}</label>
                                <input required type="text" class="form-control" name="url" id="url" placeholder="https://......">
                            </div>

                            <button type="submit" class="btn btn-primary pull-right"> {{__('message.bnt_submit')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection