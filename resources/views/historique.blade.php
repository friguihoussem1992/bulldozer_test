@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
 

             <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Temps d'accès </th>
                        <th>Lien accédé</th>
                        <th>Identifiant Utilisateur</th>
                        <th>Adresse Ip</th>
                        <th>Pays</th>
                        <th>User Agent Navigateur</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($historiques as $hist)
                    <tr>    
                            <td> #</td>
                            <td> {{$hist->created_at}}</td>
                            <td> {{$hist->url}}</td>
                            <td> {{$hist->identifiant}}</td>
                            <td> {{$hist->adresse_ip}}</td>
                            <td> {{$hist->pays}}</td>
                            <td> {{$hist->user_agent}}</td>
                    </tr>
                    @endforeach
                    @if(count($historiques)==0)
                       <tr > <td colspan="7"> Aucune donnée disponible </td></tr>
                    @endif
                </tbody>
            </table>
        </div>


         

    </div>
</div>
@endsection