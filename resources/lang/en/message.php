<?php

return [
    'welcome' => 'Welcome to our application!',
    'btn_create_raccourcir' => 'Shorten a new link',
    'bnt_supprime' => 'Delete',
    'title_modal_link' => 'Shorten a new link',
    'title_link_label' => 'Link',
    'bnt_submit' => 'Send',
    'Login' => 'Login',
    'Forgot_Your_Password' => 'Forgot Your Password ?',
    'Login' => 'Login',
    'Register'=> 'Register',
    'msg_delete' => 'Delete With success',
    'msg_created' => 'Created with success',
    'msg_atteint' => 'You have reached the maximum',
    'error_msg' => 'An error appears.',
    'Logout' => 'Logout',
    'historique'=> 'History',
    'home' => 'Home'

];
?>