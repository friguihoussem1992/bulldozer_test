<?php

return [
    'welcome' => 'Bienvenue!',
    'btn_create_raccourcir' => 'Raccourcir un nouveau lien',
    'bnt_supprime' => 'Supprimer',
    'title_modal_link' => 'Raccourcir un nouveau Lien ',
    'title_link_label' => 'Lien',
    'bnt_submit' => 'Envoyer',
    'Login' => 'Se connecter',
    'Forgot_Your_Password' => 'Mot de pass oublier ?',
    'Login' => 'Se connecter',
    'Register'=> 'Inscription',
    'msg_delete' => 'Suppression a été effectuée  avec succès',
    'msg_created' =>  'Creation a été effectuée  avec succès',
    'msg_atteint' => ' Vous avez atteint le maximum',
    'error_msg' => 'Une erreur apparaît.',
    'Logout' => 'Deconnecter',
    'historique'=> 'Historique',
    'home' => 'Acceuil'
];
?>